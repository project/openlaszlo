
OpenLaszlo

This module will create an API to allow integration between Drupal and
OpenLaszlo, available at http://openlaszlo.org. Required XML files will be
generated according to theming needs, allowing for OpenLaszlo flash files to
be created on the fly. Requires tomcat to be installed on the server.